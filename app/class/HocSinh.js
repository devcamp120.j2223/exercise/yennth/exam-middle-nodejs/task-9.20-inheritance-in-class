import ConNguoi from "./ConNguoi.js";

class HocSinh extends ConNguoi{
    constructor(hoTen, ngaySinh, queQuan, tenTruong, lop, soDienThoai){
        super(hoTen, ngaySinh, queQuan);
        this.tenTruong = tenTruong;
        this.lop = lop;
        this.soDienThoai = soDienThoai;
    }

}

export default HocSinh;