import ConNguoi from "./ConNguoi.js";

class CongNhan extends ConNguoi{
    constructor(hoTen, ngaySinh, queQuan, nganhNghe, noiLamViec, luong){
        super(hoTen, ngaySinh, queQuan);
        this.nganhNghe = nganhNghe;
        this.noiLamViec = noiLamViec;
        this.luong = luong;
    }

}

export default CongNhan;