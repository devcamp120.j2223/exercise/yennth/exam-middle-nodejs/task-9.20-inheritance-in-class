import ConNguoi from "./ConNguoi.js";
import HocSinh from "./HocSinh.js";
import SinhVien from "./SinhVien.js";
import CongNhan from "./CongNhan.js";

let person = new ConNguoi("Hoa", "20/12/2000", "HCM");
console.log(person);
let hocsinhA = new HocSinh("An","20/2/2011", "HN","Dongdo", "5", "0912312399");
console.log(hocsinhA);
let sinhvienB = new SinhVien("Bao","11/5/1999", "Hue","Dongdo", "D23", "0954645599","Maketing","41403099");
console.log(sinhvienB)
let congnhanC = new CongNhan("Cuong","30/1/1993","HN", "May", "OIU","10000000");
console.log(congnhanC)

