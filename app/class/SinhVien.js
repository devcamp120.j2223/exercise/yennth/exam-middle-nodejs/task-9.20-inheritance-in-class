import HocSinh from "./HocSinh.js";


class SinhVien extends HocSinh{
    constructor(hoTen, ngaySinh, queQuan, tenTruong, lop, soDienThoai, chuyenNganh, MSSV){
        super(hoTen, ngaySinh, queQuan, tenTruong, lop, soDienThoai);
        this.chuyenNganh = chuyenNganh;
        this.MSSV = MSSV;
    }

}

export default SinhVien;